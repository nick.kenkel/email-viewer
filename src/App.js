
// import Body from './components/containers/EmailBody';
// import ContentContainer from './components/containers/ContentContainer';
// import ContentBody from './components/containers/ContentBody';


// import ContentHeader from './components/blocks/ContentHeader';
// import WelcomeText from './components/blocks/WelcomeText';
// import PrimaryText from './components/blocks/PrimaryText';
// import ContentP from './components/blocks/ContentP';
// import PinCodeBox from './components/blocks/PinCodeBox';
// import ActionButton from './components/blocks/ActionButton';

import Template from "./components/templates/EmailTemplate"


function App() {
  return (
    <>
      <Template
        brandImage={
          'https://enotarylog.com/wp-content/uploads/2021/10/eNotaryLog_Logo_Horz_CoralSmoke-e1633707211790.png'
        }
        organization={'Organization_Name'}
        primary={'#00ff00'}
        full_name={'Adam Smith'}
        primaryText={'Your notarized document is complete.'}
        textContent={
          'To view your completed notarized document, click the button below and then enter your email and your unique PIN, provided.'
        }
        pinCode={
          'SUPER_LONG_PIN_CODE_TEXT_XSDFSDFSDF_SDFSDFSDF_SDFSDFSDFSDFS_SDFSDF'
        }
        buttonText={'RETRIEVE DOCUMENTS'}
        textShadow={true}
      />
      <br />
      <br />

      <Template
        brandImage={
          'https://enotarylog.com/wp-content/uploads/2021/10/eNotaryLog_Logo_Horz_CoralSmoke-e1633707211790.png'
        }
        organization={'Organization_Name'}
        primary={'#00ff00'}
        full_name={'Adam Smith'}
        primaryText={'Your notarized document is complete.'}
        textContent={
          'To view your completed notarized document, click the button below and then enter your email and your unique PIN, provided.'
        }
        pinCode={
          'SUPER_LONG_PIN_CODE_TEXT_XSDFSDFSDF_SDFSDFSDF_SDFSDFSDFSDFS_SDFSDF'
        }
        buttonText={'RETRIEVE DOCUMENTS'}
        textShadow={false}
      />
    </>
  );
  // return (
  //   <Body>
  //     <ContentContainer>
  //       <ContentHeader
  //         brandImage={
  //           'https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png'
  //         }
  //         organization={'eNotaryLog'}
  //       />
  //       <ContentBody>
  //         <WelcomeText full_name={'Adam Smith'} />
  //         <PrimaryText text={'Your notarized document is complete.'} />
  //         <ContentP
  //           text={
  //             'To view your completed notarized document, click the button below and then enter your email and your unique PIN, provided.'
  //           }
  //         />
  //         {/* <ContentP
  //           text={
  //             'To view your completed notarized document, click the button below and then enter your email and your unique PIN, provided.'
  //           }
  //         /> */}
  //         <PinCodeBox />
  //         <ActionButton
  //           buttonText={'CLICK ME'}
  //           primary={'#00ff00'}
  //           textShadow={true}
  //         />
  //       </ContentBody>
  //     </ContentContainer>
  //   </Body>
  // );
}

export default App;



        // <ActionButton
        //   buttonText={'WITHOUT SHADOW'}
        //   primary={''}
        //   textShadow={false}
        // />
        // <ActionButton buttonText={'WITH SHADOW'} textShadow={true} />
        // <ActionButton
        //   buttonText={'NO SHADOW'}
        //   primary={'#ffeeff'}
        //   textShadow={false}
        // />
        // <ActionButton
        //   buttonText={'SHADOW'}
        //   primary={'#ffeeff'}
        //   textShadow={true}
        // />
        // <ActionButton
        //   buttonText={'NO SHADOW'}
        //   primary={'#00ff00'}
        //   textShadow={false}
        // />