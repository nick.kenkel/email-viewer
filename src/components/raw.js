<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>{{{subject}}}</title>
    <style>
      @media only screen {
        html,
        body {
          margin: 0;
          padding: 0;
          min-height: 100%;
        }
      }

      body{
        background: #f3f3f3;
        box-sizing: border-box;

        color: #000;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 16px; 
        font-weight: 400;

        line-height: 1.575;
        margin:0;
        padding:0;
      }

      .content-container{
        margin:auto;
        display:grid;
        
        padding: 5%;
        grid-gap:10px;
      }

      .content-header{
        margin:auto;
        display:grid;
      }
      
      .content-header-image{
        margin:auto;
        max-width: 80%;
        max-height: 200px;
      }

      .content-body{
        margin: 0 auto;
        max-width: 490px;
        padding: 30px;
        background: #fff;


      }

      .content-body-h1{
        margin: 0;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 24px;
        font-weight: 400;
        line-height: 1.8;
        padding: 0;
        text-align: left;
        word-wrap: normal;
      }

      .content-body-p{
        font-family: Helvetica, Arial, sans-serif;
        font-size: 16px;
        font-weight: 400;
        line-height: 1.375;
      }

      .pinCode-box{
        background:#d8f1e3;
        background-color:rgba(0,111,131,.1);
        font-family: Helvetica,Arial,sans-serif;
        font-size:14px;
        font-weight:400;
        line-height:1.575;
        margin:0;
        margin-bottom:0;
        padding: 15px 10px;
        text-align:center;
      }

      .button-wrapper{
        
        text-align: center;

        padding:10px;
      }

      .button{
        background: #ed5f42; 
        color: #fff;
        border-radius: 3px;
        display: inline-block;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 18px;
        font-weight: 400;
        line-height: 1.575;
        padding: 10px 20px;
        text-align: center;
        text-decoration: none;

        text-shadow: 1px 1px 1px black;
      }



      .bottom-spacing{
        display: none;
        white-space: nowrap;
        font: 15px courier;
        line-height: 0;
      }


    </style>
  </head>

  <body style="
    box-sizing: border-box;
    color: #000;
    font-family: Helvetica, Arial, sans-serif;
    font-size: 16px; 
    font-weight: 400;
    line-height: 1.575;
    margin:0;
    padding:0;
    box-sizing:border-box;
">
    <div class="content-container" 
      style="
        box-sizing:border-box;
        margin:auto;
        display:grid;
        padding: 5%;
        background: #f3f3f3;
        color: #000;
        font-family: Helvetica, Arial, sans-serif;
        font-size: 16px; 
        font-weight: 400;
        line-height: 1.575;">
        <div class="content-header" 
          style="
            margin:auto;
            display:grid;
            margin-bottom:10px;">
            {{#if brandImage}}
            <img class="content-header-image" 
              style="
                margin:auto;
                max-width: 80%;
                max-height: 200px;"
              alt={{organization}}
              src={{brandImage}}
            />
            {{else}}
            <h1 class="content-header-text" style="
              font-family: Helvetica, Arial, sans-serif;">
              {{organization}}</h1>
            {{/if}}

        </div>

      <div class="content-body" 
        style="margin: 0 auto;
          max-width: 490px;
          padding: 30px;
          background: #fff;">
        <h1 class="content-body-h1" 
          style="
            margin: 0;
            font-family: Helvetica, Arial, sans-serif;
            font-size: 24px;
            font-weight: 400;
            line-height: 1.8;
            padding: 0;
            text-align: left;
            word-wrap: normal;">
          Hello <strong>{{full_name}}</strong>,
        </h1>

        <h1 class="content-body-h1" 
          style="
            margin: 0;
            font-family: Helvetica, Arial, sans-serif;
            font-size: 24px;
            font-weight: 400;
            line-height: 1.8;
            padding: 0;
            text-align: left;
            word-wrap: normal;">
          Your notarized document is complete.
        </h1>
        <p class="content-body-p" 
          style="
            font-family: Helvetica, Arial, sans-serif;
            font-size: 16px;
            font-weight: 400;
            line-height: 1.375;">To view your completed notarized document, click the button below and then enter your email and your unique PIN, provided.
        </p>
        <p class="pinCode-box"
          style="
            background:#d8f1e3;
            background-color:rgba(0,111,131,.1);
            font-family: Helvetica,Arial,sans-serif;
            font-size:14px;
            font-weight:400;
            line-height:1.575;
            margin:0;
            margin-bottom:0;
            padding: 15px 10px;
            text-align:center;
            word-wrap:break-word;
            word-break:break-all;
            ">
          <strong>PIN:<br>{{{ns_document_delivery_pin}}}</strong>
        </p>

        <div class="button-wrapper" 
          style="
            text-align: center;
            padding:10px;">
          <a
            class="button"
            href={{{ns_document_delivery_url}}}
            style="
              {{#if primary}}
              background: {{primary}};
              {{else}}
              background: #ed5f42; 
              {{/if}}
              color: #fff;
              border-radius: 3px;
              display: inline-block;
              font-family: Helvetica, Arial, sans-serif;
              font-size: 18px;
              font-weight: 400;
              line-height: 1.575;
              padding: 10px 20px;
              text-align: center;
              text-decoration: none;

              text-shadow: 1px 1px 1px black;
            "
            target="_blank"
          >
            RETRIEVE DOCUMENTS
          </a>
        </div>
      </div>
      
      <!-- prevent Gmail on iOS font size manipulation -->
      <div class="bottom-spacing" 
        style="
          display: none;
          white-space: nowrap;
          font: 15px courier;
          line-height: 0;">
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
      </div>
    </div>
  </body>
</html>