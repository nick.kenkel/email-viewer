
import Body from '../containers/EmailBody';
import ContentContainer from '../containers/ContentContainer';
import ContentBody from '../containers/ContentBody';
import ContentHeader from '../blocks/ContentHeader';
import WelcomeText from '../blocks/WelcomeText';
import PrimaryText from '../blocks/PrimaryText';
import ContentP from '../blocks/ContentP';
import PinCodeBox from '../blocks/PinCodeBox';
import ActionButton from '../blocks/ActionButton';

function EmailTemplate(props) {
  return (
    <Body style={props.bodyStyle}>
      <ContentContainer>
        <ContentHeader
          brandImage={props.brandImage}
          organization={props.organization}
        />

        <ContentBody>
          <WelcomeText full_name={props.full_name}/>
          <PrimaryText text={props.primaryText} />
          <ContentP
            text={props.textContent}
          />
          <PinCodeBox pinCode={props.pinCode} />
          <ActionButton
            buttonText={props.buttonText}
            primary={props.primary}
            textShadow={props.textShadow}
          />
        </ContentBody>
      </ContentContainer>
    </Body>
  );
}

export default EmailTemplate;



        // <ActionButton
        //   buttonText={'WITHOUT SHADOW'}
        //   primary={''}
        //   textShadow={false}
        // />
        // <ActionButton buttonText={'WITH SHADOW'} textShadow={true} />
        // <ActionButton
        //   buttonText={'NO SHADOW'}
        //   primary={'#ffeeff'}
        //   textShadow={false}
        // />
        // <ActionButton
        //   buttonText={'SHADOW'}
        //   primary={'#ffeeff'}
        //   textShadow={true}
        // />
        // <ActionButton
        //   buttonText={'NO SHADOW'}
        //   primary={'#00ff00'}
        //   textShadow={false}
        // />