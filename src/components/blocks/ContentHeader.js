



const HeaderImage = (props)=>{
  return (
    <img
      className='content-header-image'
      src={props.brandImage}
      alt={props.organization}
    />
  );
}
const HeaderText = (props) => {
  console.log(props)
  return (
    <h1
      class='content-header-text'
      style={{
        fontFamily: 'Helvetica, Arial, sans-serif',
      }}
    >
      {props.organization}
    </h1>
  );
}

const ContentHeader = ({organization="eNotaryLog", ...props}) => {
    return (
      <div
        className='content-header'
        style={{
          display: 'grid',
          margin: 'auto',
          marginBottom: 10,
        }}
      >
        {props.brandImage ? (
          <HeaderImage organization={organization} {...props} />
        ) : (
          <HeaderText organization={organization} {...props} />
        )}
      </div>
    );

}


export default ContentHeader