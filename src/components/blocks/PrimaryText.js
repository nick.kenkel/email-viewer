

const PrimaryText = ({text="Primary Text"
}) =>{
    return (
      <h1
        style={{
          fontFamily: 'Helvetica, Arial, sans-serif',
          fontSize: 24,
          fontWeight: 400,
          
          lineHeight: 1.8,
          margin: 0,
          padding: 0,

          textAlign: 'left',
          wordWrap: 'normal',
        }}
      >
        {text}
      </h1>
    );
}


export default PrimaryText;