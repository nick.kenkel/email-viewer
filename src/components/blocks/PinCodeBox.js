

const PinCodeBox = ({pinCode="{{ns_document_delivery_pin}}"}) => {
  return (
    <p
      className='pinCode-box'
      style={{
        background: '#d8f1e3',
        backgroundColor: 'rgba(0,111,131,.1)',

        fontFamily: 'Helvetica, Arial, sans-serif',
        fontSize: 14,
        fontWeight: 400,

        lineHeight: 1.575,
        margin: 0,
        padding: '15px 10px',

        textAlign: 'center',

        wordWrap: 'break-word',
        wordBreak: 'break-all',
      }}
    >
      <strong>
        PIN:
        <br />
        {pinCode}
      </strong>
    </p>
  );
};


export default PinCodeBox;