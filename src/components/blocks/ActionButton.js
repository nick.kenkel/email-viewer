const ActionButton = ({buttonText="BUTTON TEXT", textShadow=true, ...props}) => {
  

  return (
    <div
      className='button-wrapper'
      style={{
        textAlign: 'center',
        padding: '10px',
      }}
    >
      <a
        className='action-button'
        rel='noreferrer'
        href={props.href}
        style={{
          background: props.primary || '#ed5f42',
          borderRadius: 3,

          color: '#fff',
          display: 'inline-block',

          fontFamily: 'Helvetica, Arial, sans-serif',
          fontSize: 18,
          fontWeight: 400,

          lineHeight: 1.575,
          padding: '10px 20px',

          textAlign: 'center',
          textDecoration: 'none',
          textShadow: textShadow ? '1px 1px 1px black' : null,

          ...props.style,
        }}
        target='_blank'
      >
        {buttonText}
      </a>
    </div>
  );
}


export default ActionButton

