const ContentP = ({text="ContentP Text"}) => {
    return <p className="content-body-p"
        style={{
            fontFamily: 'Helvetica, Arial, sans-serif',
            fontSize: 16,
            fontWeight: 400,

            lineHeight: 1.375,
        }}
    >
    {text}
    </p>
}


export default ContentP