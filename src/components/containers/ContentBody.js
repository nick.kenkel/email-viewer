const ContentBody = (props) => {
  return (
    <div
        style={{
          background: '#fff',
          margin: '0 auto',
          maxWidth: 490,
          padding: 30,
      }}
    >
      {props.children}
    </div>
  );
};

export default ContentBody;
