


const ContentContainer = (props) => {
    return (
      <div
        style={{
          background: '#f3f3f3',
          boxSizing: 'border-box',

          color: '#000',
          display: 'grid',
          
          fontFamily: 'Helvetica, Arial, sans-serif',
          fontSize: 16,
          fontWeight: 400,
          
          lineHeight: 1.575,
          margin: 'auto',
          padding: '5%',
        }}
      >
        {props.children}
      </div>
    );
}


export default ContentContainer