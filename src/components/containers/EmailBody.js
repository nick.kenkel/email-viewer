const EmailBody = (props) => {
  return (
    <body
      style={{
        boxSizing: 'border-box',
        color: '#000',
        fontFamily: 'Helvetica, Arial, sans-serif',
        fontSize: 16,
        fontWeight: 400,
        lineHeight: 1.575,
        margin: 0,
        padding: 0,
      }}
    >
      {props.children}
    </body>
  );
};

export default EmailBody;
