

import React from 'react';
import SendGridTemplate from "../../components/templates/EmailTemplate"

export default {
  title: 'Templates/Model',
  component: SendGridTemplate,
    argTypes: {
        buttonText:{control:'text'},
  },
}



const Template = (args) => <SendGridTemplate {...args} />;

export const DocumentRetrival = Template.bind({});
DocumentRetrival.args = {
  //Dynamic
  brandImage:'https://enotarylog.com/wp-content/uploads/2021/10/eNotaryLog_Logo_Horz_CoralSmoke-e1633707211790.png',
  buttonText:'RETRIEVE DOCUMENTS',    
  organization:'Organization Name',
  primary:'#000fff',
  full_name:'Adam Smith',  
  pinCode:'PIN CODE',


  //Static
  primaryText:'Your notarized document is complete.',
  textContent:'To view your completed notarized document, click the button below and then enter your email and your unique PIN, provided.'
};

export const NotarizationRequest = Template.bind({});
NotarizationRequest.args = {
  //Dynamic
  brandImage:'https://enotarylog.com/wp-content/uploads/2021/10/eNotaryLog_Logo_Horz_CoralSmoke-e1633707211790.png',
  buttonText:'CONTINUE',    
  organization:'Organization Name',
  primary:'#00ff00',
  full_name:'Adam Smith',
  pinCode:'PIN CODE',

  //Static
  primaryText:'eNotaryLog has requested a notarization.',
  textContent:'The PIN below is required multiple times during your session to identify yourself. We recommend writing it down and having it in front of you throughout your session.'
};
