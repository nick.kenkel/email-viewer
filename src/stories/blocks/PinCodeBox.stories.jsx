import React from 'react';
import PinCodeBox from '../../components/blocks/PinCodeBox';

export default {
  title: 'Blocks/PinCodeBox',
  component: PinCodeBox,
    argTypes: {
        text:{control:'text'},
  },
}



const Template = (args) => <PinCodeBox {...args} />;

export const LongPin = Template.bind({});
LongPin.args = {
  pinCode:'document_delivery_pin_abcdefghijklmnop_abcd_abcd_abcd',
};

export const ShortPin = Template.bind({});
ShortPin.args = {
  pinCode:'123456',
};
