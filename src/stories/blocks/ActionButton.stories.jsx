import React from 'react';
import ActionButton from '../../components/blocks/ActionButton';

export default {
  title: 'Blocks/ActionButton',
  component: ActionButton,
    argTypes: {
    primary: { control: 'color' },
  },
}



const Template = (args) => <ActionButton {...args} />;

export const DefaultShadow = Template.bind({});
DefaultShadow.args = {
  primary: '',
  buttonText: 'Default - Shadow',  
  textShadow:true,  
  style: {}
};

export const DefaultNoShadow = Template.bind({});
DefaultNoShadow.args = {
  primary: '',
  buttonText: 'Default - No Shadow',
  textShadow:false,  
  style: {}

};

export const PrimaryShadow = Template.bind({});
PrimaryShadow.args = {
  primary: '#00ff00',
  buttonText: 'OrgPrimaryShadow',
  textShadow:true,  
  style: {}

};

export const PrimaryNoShadow = Template.bind({});
PrimaryNoShadow.args = {
  primary: '#00ff00',
  buttonText: 'OrgPrimaryShadow',
  textShadow:false,
  style: {}
};