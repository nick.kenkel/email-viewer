import React from 'react';
import ContentHeader from '../../components/blocks/ContentHeader';

export default {
  title: 'Blocks/ContentHeader',
  component: ContentHeader,
    argTypes: {
        brandImage:{control:'text'},
        organization: { control:'text' },
  },
}



const Template = (args) => <ContentHeader {...args} />;

export const NoLogoImage = Template.bind({});
NoLogoImage.args = {
  organization:'Organization Name',
  brandImage:'',
};


export const LogoImage = Template.bind({});
LogoImage.args = {
  organization:'Organization',
  brandImage: 'https://enotarylog.com/wp-content/uploads/2021/10/eNotaryLog_Logo_Horz_CoralSmoke-e1633707211790.png',
};
