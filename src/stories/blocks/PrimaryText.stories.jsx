import React from 'react';
import PrimaryText from '../../components/blocks/PrimaryText';

export default {
  title: 'Blocks/PrimaryText',
  component: PrimaryText,
    argTypes: {
        text:{control:'text'},
  },
}



const Template = (args) => <PrimaryText {...args} />;

export const HelloFullName = Template.bind({});
HelloFullName.args = {
  text:'Your notarized document is complete.',
};


