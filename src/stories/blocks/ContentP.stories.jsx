import React from 'react';
import ContentP from '../../components/blocks/ContentP';

export default {
  title: 'Blocks/ContentP',
  component: ContentP,
    argTypes: {
        text:{control:'text'},
  },
}



const Template = (args) => <ContentP {...args} />;

export const CompletionDocuments = Template.bind({});
CompletionDocuments.args = {
  text:'To view your completed notarized document, click the button below and then enter your email and your unique PIN, provided.',
};


