import React from 'react';
import WelcomeText from '../../components/blocks/WelcomeText';

export default {
  title: 'Blocks/WelcomeText',
  component: WelcomeText,
    argTypes: {
        full_name:{control:'text'},
  },
}



const Template = (args) => <WelcomeText {...args} />;

export const HelloFullName = Template.bind({});
HelloFullName.args = {
  full_name:'George Washington',
};


